package com.arash.altafi.tvonline.model

data class PlayList(
    val name: String? = null,
    val image: String? = null,
    val url: String? = null,
)