package com.arash.altafi.tvonline.ui

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.arash.altafi.tvonline.databinding.ItemPlayerBinding
import com.arash.altafi.tvonline.model.PlayList
import com.bumptech.glide.Glide

class PlayerAdapter: RecyclerView.Adapter<PlayerAdapter.ViewHolder>() {

    private lateinit var list: List<PlayList>

    fun setPlayList(_list: List<PlayList>) {
        list = _list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding = ItemPlayerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val playList: PlayList = list[position]
        holder.bind(playList)
    }

    override fun getItemCount(): Int = list.size

    inner class ViewHolder(private val itemPlayerBinding: ItemPlayerBinding): RecyclerView.ViewHolder(itemPlayerBinding.root) {
        fun bind(playList: PlayList) {
            itemPlayerBinding.apply {
                Glide.with(root.context).load(playList.image).into(ivChannel)
                tvChannel.text = playList.name
                llItem.setOnClickListener {
                    val intent = Intent(root.context, MediaPlayerActivity::class.java)
                    intent.putExtra("name", playList.name)
                    intent.putExtra("url", playList.url)
                    root.context.startActivity(intent)
                }
            }
        }
    }

}

