package com.arash.altafi.tvonline.ui

import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import androidx.appcompat.widget.AppCompatImageView
import com.arash.altafi.tvonline.R
import com.arash.altafi.tvonline.databinding.ActivityMediaPlayerBinding
import com.arash.altafi.tvonline.utils.*
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector

class MediaPlayerActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMediaPlayerBinding
    private var player: SimpleExoPlayer? = null
    private var isFullScreen = false
    private lateinit var url: String
    private lateinit var name: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMediaPlayerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        name = intent.getStringExtra("name").toString()
        url = intent.getStringExtra("url").toString()

        supportActionBar?.title = name
        initializeVideo(name, url)
    }

    private fun initializeVideo(name: String, url: String) {
        binding.apply {
            val fullScreen: AppCompatImageView = videoPlayer.findViewById(R.id.exo_fullscreen_button)
            val speedBtn: AppCompatImageView = videoPlayer.findViewById(com.google.android.exoplayer2.R.id.exo_playback_speed)

            val trackSelector = DefaultTrackSelector(this@MediaPlayerActivity)
            player = SimpleExoPlayer.Builder(this@MediaPlayerActivity)
                .setTrackSelector(trackSelector)
                .setSeekBackIncrementMs(5000)
                .setSeekForwardIncrementMs(10000)
                .build()

            player?.initialize(this@MediaPlayerActivity, videoPlayer, name, url)

            fullScreen.setOnClickListener {
                if (isFullScreen) {
//                    toolbar.toShow()
                    requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                    window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
                    window.clearFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                    window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                    isFullScreen = false
                } else {
//                    toolbar.toGone()
                    requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                    window.setFlags(
                        WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS
                    )
                    window.setFlags(
                        WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                        WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
                    )
                    isFullScreen = true
                }
            }

            speedBtn.setOnClickListener {
                player?.speedDialog(this@MediaPlayerActivity)
            }

            player?.addListener(object : Player.Listener {
                override fun onPlaybackStateChanged(state: Int) {
                    when (state) {
                        Player.STATE_READY -> {
                            progressBar.toGone()
                            player?.playWhenReady = true
                            Log.i("test123321", "STATE_READY")
                        }
                        Player.STATE_BUFFERING -> {
                            progressBar.toShow()
                            videoPlayer.keepScreenOn = true
                            Log.i("test123321", "STATE_BUFFERING")
                        }
                        Player.STATE_IDLE -> {
                            finish()
                            Log.i("test123321", "STATE_IDLE")
                        }
                        Player.COMMAND_INVALID -> {
                            finish()
                            Log.i("test123321", "COMMAND_INVALID")
                        }
                        Player.STATE_ENDED -> {
                            Log.i("test123321", "STATE_ENDED")
                        }
                        else -> {
                            progressBar.toGone()
                            player?.playWhenReady = true
                        }
                    }
                }
            })
        }
    }

    override fun onPause() {
        super.onPause()
        player?.pause()
    }

    override fun onStart() {
        super.onStart()
        player?.play()
    }

    override fun onStop() {
        super.onStop()
        player?.stop()
    }

    override fun onResume() {
        super.onResume()
        player?.play()
    }

    override fun onDestroy() {
        super.onDestroy()
        player?.release()
        player?.pause()
    }

}