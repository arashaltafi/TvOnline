package com.arash.altafi.tvonline.utils

import android.content.Context
import com.arash.altafi.tvonline.R

object Utils {

    fun speedMedia(context: Context) =
        arrayOf(
            context.getString(R.string.speed_media).applyValue("0/25"),
            context.getString(R.string.speed_media).applyValue("0/5"),
            context.getString(R.string.speed_media).applyValue("0/75"),
            context.getString(R.string.normal),
            context.getString(R.string.speed_media).applyValue("1/25"),
            context.getString(R.string.speed_media).applyValue("1/5"),
            context.getString(R.string.speed_media).applyValue("2")
        )

}