package com.arash.altafi.tvonline.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.arash.altafi.tvonline.databinding.ActivityMainBinding
import com.arash.altafi.tvonline.model.PlayList

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var playListAdapter: PlayerAdapter = PlayerAdapter()
    private var playList: ArrayList<PlayList> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
    }

    private fun init() {

        //Initial Model
//        playList.add(PlayList("شبکه 1", "https://sepehr.irib.ir/uploads/channel/1.png", "https://s1-cloud.irib.ir/securelive3/tv1hd/1080p.m3u8?s=iPuBuikfSgrA2AY8Sv-p5g&t=1657977629"))
//        playList.add(PlayList("شبکه 2", "https://sepehr.irib.ir/uploads/channel/2.png", "https://s1-cloud.irib.ir/securelive3/tv2hd/1080p.m3u8?s=u7VIT71LP0HppTeiOhUb3g&t=1657977803"))
//        playList.add(PlayList("شبکه 3", "https://sepehr.irib.ir/uploads/channel/3.png", "https://s1-cloud.irib.ir/securelive3/tv3hd/tv3hd.m3u8?s=lzvs5SmFjGMGzO39SwI2ug&t=1658002673"))
//        playList.add(PlayList("شبکه 4", "https://sepehr.irib.ir/uploads/channel/4-min.png", "https://s-cloud.irib.ir/securelive3/tv4sd/576p.m3u8?s=WmYOWe_OUFNjUQYpnSaPiQ&t=1657984001"))
//        playList.add(PlayList("شبکه 5", "https://sepehr.irib.ir/uploads/channel/iribtv5_min.png", "https://s-cloud.irib.ir/securelive3/tv5sd/576p.m3u8?s=tkzVXywXFbz79qH0G0n3hA&t=1657984042"))
//        playList.add(PlayList("شبکه خبر", "https://sepehr.irib.ir/uploads/channel/khabar-min.png", "https://s-cloud.irib.ir/securelive3/irinnhd/1080p.m3u8?s=tpAB_DHMhjrnIbqA2vPFOQ&t=1657984098"))
        playList.add(PlayList("PMC", "https://is4-ssl.mzstatic.com/image/thumb/Purple114/v4/6f/5d/c1/6f5dc1b1-d218-cf90-a836-6f7d373c7bcd/App_Icon_-_Small-marketing.lsr/600x600bb.jpg", "https://hls.pmchd.live/hls/stream.m3u8"))
        playList.add(PlayList("RadioJavan", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRUYvoniuqYP2cFkwMdOiPilJyn-t6qAR_y6mFBA72btQ0NSjhrqj5uoMjUP1gWXQQ3WHM&usqp=CAU", "https://stream.rjtv.stream/live/smil:rjtv.smil/chunklist_w1246774756_b1992000.m3u8"))
        playList.add(PlayList("ManoTo", "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/Manoto-Logo.png/640px-Manoto-Logo.png", "https://edge-uk2.manoto.click/live_500.m3u8"))
        playList.add(PlayList("Iran International", "https://paykanartcar.com/wp-content/uploads/Iran-International-Channel.png", "https://dev-live.livetvstream.co.uk/LS-63503-4/chunklist_b1196000.m3u8"))
        playList.add(PlayList("VOA", "https://images-na.ssl-images-amazon.com/images/I/618dCg7mNpL.png", "https://voa-ingest.akamaized.net/hls/live/2033876/tvmc07/VOA-TVMC07_0360/chunklist.m3u8"))
        playList.add(PlayList("BBC", "https://download.logo.wine/logo/BBC_News/BBC_News-Logo.wine.png", "https://vs-hls-pushb-ww-live.akamaized.net/x=3/i=urn:bbc:pips:service:bbc_persian_tv/t=3840/v=pv5/b=437056/main.m3u8"))
        playList.add(PlayList("Pars TV", "https://lh3.googleusercontent.com/MKbKNI9i0sXX6Y9ewKt5AARhd0u7oO42bf3N56wRhBJ8r8x_rle7bzpqhzr6iHEmu-s=w800-h500", "https://livestream.5centscdn.com/cls032817/18e2bf34e2035dbabf48ee2db66405ce.sdp/chunks.m3u8"))
        playList.add(PlayList("ITN TV", "https://s6.uupload.ir/files/download_3bn.jpg", "https://2nbyjjx7y53k-hls-live.5centscdn.com/itnapp/4e0ea63032b868402b61d3a35e7ca168.sdp/chunks.m3u8"))

        playListAdapter.setPlayList(playList)
        binding.rvPlayer.apply {
            adapter = playListAdapter
            layoutManager = GridLayoutManager(applicationContext, 3)
        }
    }

}